#!/usr/bin/env python3
import glob
import argparse
import datetime
import os
from zipfile import ZipFile
import multiprocessing
from functools import partial
import logging
import json
import sys
from sentinelsat import SentinelAPI, geojson_to_wkt
import tempfile
from dateutil import parser as date_parser
import xarray as xr
import rasterio
import affine
from rasterio.warp import transform
from pyproj import Proj
import numpy as np
import subprocess
from pyresample import kd_tree
from pyresample.geometry import AreaDefinition, SwathDefinition
import shutil


def convert_coords(rcoords, crs):
    """
    Converts requested coordinates to requested projection
    """
    trg_proj = Proj(crs)
    xy_list = [trg_proj(c[0], c[1]) for c in rcoords[0]]
    x_array = np.array([c[0] for c in xy_list])
    y_array = np.array([c[1] for c in xy_list])
    return x_array, y_array

def affine_transformation(rcoords, res, crs):
    """
    Creates affine-style geotransformation
    """
    a_pixel_width = res
    b_rotation    = 0
    d_column_rotation = 0
    e_pixel_height = res
    crs = str(crs).upper()
    (x_array, y_array) = convert_coords(rcoords, crs)
    c_x_ul = x_array.min()
    f_y_ul = y_array.max()
    height = (y_array.max() - y_array.min()) / res
    width = (x_array.max() - x_array.min()) / res

    aff = affine.Affine(a_pixel_width,
            b_rotation,
            c_x_ul,
            d_column_rotation,
            -1 * e_pixel_height,
            f_y_ul
            )
    return aff, height, width

def create_empty_dst(output_dst, rcoords, res, crs, dtype):
    """
    Creates empty geotiff-dataset as boundaries for the
    satellite data
    """
    aff, height, width = affine_transformation(rcoords, res, crs)

    rasterio.open(output_dst,
            'w',
            driver='GTiff',
            height=height,
            width=width,
            count=1,
            dtype=dtype,
            crs=crs,
            transform=aff,
            photometric='RGB',
            nodata=-999,
            compress='LZW',
            predict=2
            )
    return True

def download_sentinel_product(
                        product_key,
                        products,
                        api,
                        output_path
                    ):

    print("Obtaining item {}".format(product_key))
    api.download(product_key, directory_path=output_path)

def extract_nc(output_path, tmp_dir):

    nc_fname = 'chl_oc4me.nc'
    with tempfile.TemporaryDirectory() as unzip:
        for file in os.listdir(tmp_dir):
            with ZipFile(os.path.join(tmp_dir, file)) as zip_ref:
                zip_ref.extractall(str(unzip))

        for file in os.listdir(unzip):
            fname   = str(file.strip('.SEN3/'))
            _chla = xr.open_dataset(str(unzip +'/' + file + '/chl_oc4me.nc'))
            _chla = 10**(_chla)
            _chla = _chla.fillna(-999)
            _coords = xr.open_dataset(str(unzip + '/' + file + '/geo_coordinates.nc'))
            dst = xr.Dataset(
                    {
                        "chl_oc4me":(
                            ["x","y"],
                            _chla.CHL_OC4ME.values
                            ),
                        },
                    coords={
                        "lon":(
                            ["x", "y"],
                            _coords.longitude.values
                            ),
                        "lat":(
                            ["x", "y"],
                            _coords.latitude.values
                            ),
                        }
                    )
            dst.attrs = _chla.CHL_OC4ME.attrs
            dst.to_netcdf(str(output_path + '/' + fname + '_' + nc_fname))

def extract_time(dst):
    """
    Splits CODA filenames at timestamp
    """
    timestamp = dst.split('_')[7]

    return timestamp

def resample_data(output_dst, output_path, request):
    """
    Function that resamples the netcdf data to an empty GeoTiff
    and applies a colormap
    """
    rcoords = request['roi']['coordinates']
    res = request['spatial_resolution']
    crs = request['crs']

    # Create area definition:
    area_id = 'req'
    projection = crs
    resolution = res
    (x_array, y_array) = convert_coords(rcoords, crs)
    height = (y_array.max() - y_array.min()) / resolution
    width = (x_array.max() - x_array.min()) / resolution
    shape = [int(height), int(width)]
    upper_left_extent = [x_array.min(), y_array.max()]
    area_def = AreaDefinition.from_ul_corner(
            area_id=area_id,
            projection=projection,
            upper_left_extent=upper_left_extent,
            resolution=resolution,
            shape=shape
            )

    # Resample netcdf files and create geotiff
    result_file_false = False
    sorted_list = glob.glob(
            os.path.join(output_path, "*.nc")
    )
    sorted_list.sort(key = extract_time)
    for file in sorted_list:
        dst = xr.open_dataset(file)
        print('Resampling dataset: ', file)
        chl_a = dst['chl_oc4me'].values
        lon = dst['lon'].values
        lat = dst['lat'].values

        swath_def = SwathDefinition(
                lons=lon,
                lats=lat
                )
        current_result = kd_tree.resample_nearest(
                swath_def,
                chl_a,
                area_def,
                fill_value=-999,
                radius_of_influence=5000,
                nprocs=4
                )
        if result_file_false:
            result = np.where(
                    current_result!=-999,
                    current_result,
                    result
                    )
        else:
            result = current_result
            result_file_false = True
    # Create empty geotiff
    create_empty_dst(
            output_dst,
            rcoords,
            res,
            crs,
            rasterio.float32
            )
    with rasterio.open(output_dst, 'r+') as o_dst:
        o_dst.update_tags(
                START_TIME = str(extract_time(sorted_list[0])),
                END_TIME = str(extract_time(sorted_list[-1])),
                UNITS = 'mg.m-3'
                )
        o_dst.write(result, 1)

def remove_dir(path):
    for root, dirs, files in os.walk(path):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))
        os.rmdir(root)

def main():

    p = argparse.ArgumentParser()
    p.add_argument("--log-file", default=None)
    p.add_argument("--output-file", default=None)
    p.add_argument("--request-file", default=None)
    p.add_argument("-k", "--keep-temporary", action="store_true")

    args = p.parse_args()

    USER_NAME = os.environ['CODA_USER']
    USER_PASS = os.environ['CODA_PASS']

    global logger

    logger = logging.getLogger('vixed')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s  %(name)s  %(levelname)s: %(message)s')

    file_handler = logging.FileHandler(args.log_file)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)


    if args.request_file is not None:
        global request
        request = json.loads(open(args.request_file).read())
    else:
        logger.info("Request file missing, aborting")
        raise Exception("Can't proceed without request json file")

    api_url = request['api_url']
    api = SentinelAPI(
            USER_NAME,
            USER_PASS,
            api_url=api_url,
            show_progressbars=True
            )

    footprint = geojson_to_wkt(request['roi'])
    logger.debug("Search footprint is: {}".format(footprint))

    output_file = args.output_file
    output_dir = output_file + ".d"
    try:
        if request['end_time']:
            end_time = date_parser.parse(request['end_time'])
    except:
        end_time = datetime.datetime.utcnow()

    start_time = end_time - datetime.timedelta(hours=request['time_delta_hours'])
    products = api.query(
        footprint,
        date = (start_time, end_time),
        platformname=request['platformname'],
        producttype=request['producttype'],
        timeliness= request['timeliness'],
        instrumentshortname=request['instrumentshortname']
    )

    n_scenes = len(products.keys())
    message = "Found {} scenes".format(n_scenes)
    logger.info(message)
    if n_scenes>0:
        logger.info("Scenes list:\n{}".format(
            "\n".join(['\t'+products[key]['identifier'] for key in products.keys()])))
    else:
        logger.debug(" ".join(['No satellites scenes found',
                              'within last {} hours.'.format(request["time_delta_hours"])]))
        raise SystemExit("Aborting.")

    if len(products) == 0:
        logger.critical("No scenes found, exiting.")
        sys.exit(0)

    tmp_dir = os.path.join(output_dir, 'tmp_dir')
    # create directories for storing raw and tmp files
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    if not os.path.exists(tmp_dir):
        os.mkdir(tmp_dir)

    pool = multiprocessing.Pool(processes=1)
    output = pool.map(
            partial(
                download_sentinel_product,
                products=products,
                api=api,
                output_path=tmp_dir
                ),
            products.keys()
            )
    extract_nc(output_dir, tmp_dir)

    resample_data(output_file, output_dir, request)

    if args.keep_temporary is False:
        print('Removing output directory {}'.format(output_dir))
        remove_dir(output_dir)

if __name__ == "__main__":
    main()
