#!/bin/bash
set -x # quit on any errors

# USAGE:
#SHUB_USER=user SHUB_PASS=pass ./test-processor.sh settings.json output.tif

# execute processor with settings.json as input and out.tif as output
SETTINGS_FILE=$1 # first argument to this script
OUTPUT_FILE=$2 # second argument
KEEP_TEMP=${3} #Keep temporaty true/false

# path to processor can be either full or relative (relative to this script)
# relative is probably better, so script can be run on various machines
# if this script and processor are in the same directory then ./green-ice.py will do
PROCESSOR=./processor-sentinel.py

if $KEEP_TEMP; then
    # Run python executable (processor)
    $PROCESSOR --output-file=$OUTPUT_FILE --log-file=${OUTPUT_FILE%.*}-log.txt --request-file=$SETTINGS_FILE --k
else
    $PROCESSOR --output-file=$OUTPUT_FILE --log-file=${OUTPUT_FILE%.*}-log.txt --request-file=$SETTINGS_FILE
fi

# Run test script
# script should exit with status code 1 in case of failure
# like sys.exit(1) or raise exception
./test-processor-sentinel.py $SETTINGS_FILE ${OUTPUT_FILE}

